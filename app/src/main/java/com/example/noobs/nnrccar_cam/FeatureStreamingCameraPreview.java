package com.example.noobs.nnrccar_cam;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.List;


class FeatureStreamingCameraPreview extends ViewGroup implements SurfaceHolder.Callback,
        Camera.PreviewCallback {
    private final String TAG = "FSPreview";

    SurfaceView mSurfaceView;
    SurfaceHolder mHolder;
    Camera.Size mPreviewSize;
    List<Camera.Size> mSupportedPreviewSizes;
    Camera mCamera;
    Context mContext;

    private byte[] pixels = null;
    private float[] accelerometerFeatures = new float[3];
    private FeatureStreamer fs;

    FeatureStreamingCameraPreview(Context context, FeatureStreamer fs, SurfaceView sv) {
        super(context);
        this.fs = fs;
        mContext = context;

        mSurfaceView = sv;
        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void updateAccelerometerFeatures(float[] features) {
        synchronized (this) {
            accelerometerFeatures = features;
        }
    }

    public void setCamera(Camera camera) {
        mCamera = camera;
        if (mCamera != null) {
            mSupportedPreviewSizes = mCamera.getParameters()
                    .getSupportedPreviewSizes();
            requestLayout();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(),
                heightMeasureSpec);

        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getMinimumPreviewSize(mSupportedPreviewSizes, width,
                    height);
            setMeasuredDimension(mPreviewSize.width, mPreviewSize.height);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed && getChildCount() > 0) {
            final View child = getChildAt(0);

            final int width = r - l;
            final int height = b - t;

            int previewWidth = width;
            int previewHeight = height;
            if (mPreviewSize != null) {
                previewWidth = mPreviewSize.width;
                previewHeight = mPreviewSize.height;
            }

            // Center the child SurfaceView within the parent.
            if (width * previewHeight > height * previewWidth) {
                final int scaledChildWidth = previewWidth * height / previewHeight;
                child.layout((width - scaledChildWidth) / 2, 0,
                        (width + scaledChildWidth) / 2, height);
            } else {
                final int scaledChildHeight = previewHeight * width / previewWidth;
                child.layout(0, (height - scaledChildHeight) / 2, width,
                        (height + scaledChildHeight) / 2);
            }
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, acquire the camera and tell it where
        // to draw.
        try {
            if (mCamera != null) {
                mCamera.setPreviewDisplay(holder);
                mCamera.setPreviewCallback(this);
            }
        } catch (IOException exception) {
            Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        if (mCamera != null) {
            mCamera.stopPreview();
        }
    }

    private Camera.Size getMinimumPreviewSize(List<Camera.Size> sizes, int w, int h) {
        if (sizes == null)
            return null;
        int minWidth = Integer.MAX_VALUE;

        Camera.Size optimalSize = null;
        // Try to find the min size
        for (Camera.Size size : sizes) {
            if (size.width < minWidth) {
                optimalSize = size;
                minWidth = size.width;
            }
        }

        return optimalSize;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);

        mCamera.setDisplayOrientation(90);
        pixels = new byte[mPreviewSize.width * mPreviewSize.height];
        requestLayout();
        mCamera.setParameters(parameters);
        mCamera.startPreview();
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {

        if (data.length >= mPreviewSize.width * mPreviewSize.height) {
            decodeYUV420SPGrayscale(pixels, data, mPreviewSize.width,
                    mPreviewSize.height);
            synchronized (this) {
                fs.sendFeatures(mPreviewSize.width, mPreviewSize.height, pixels, accelerometerFeatures);
            }
        }

    }

    static public void decodeYUV420SPGrayscale(byte[] rgb, byte[] yuv420sp,
                                               int width, int height) {
        final int frameSize = width * height;

        for (int pix = 0; pix < frameSize; pix++) {
            int pixVal = (0xff & ((int) yuv420sp[pix])) - 16;
            if (pixVal < 0)
                pixVal = 0;
            if (pixVal > 255)
                pixVal = 255;
            rgb[pix] = (byte) pixVal;
        }
    }


}
