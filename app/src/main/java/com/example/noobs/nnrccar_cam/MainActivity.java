package com.example.noobs.nnrccar_cam;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;


/**
 * Part of NNRCCar - a self driving radio controlled car.
 * <p>
 * This activity implements an app which grabs camera preview frames (at 176x144 resolution)
 * and streams them via a TCP connection to a server app running on port 6666 on the IP address
 * provided by the user at start up.
 */
public class MainActivity extends Activity implements SensorEventListener {

    private FeatureStreamingCameraPreview mPreview;
    Camera mCamera;
    int numberOfCameras;
    int cameraCurrentlyLocked;

    // The first rear facing camera
    int defaultCameraId;

    private static final String PREFS_NAME = "org.davidsingleton.NNRCCar";

    private FeatureStreamer fs = new FeatureStreamer();
    private SensorManager sensorManager;
    private float[] gravity = new float[3];
    private float[] linear_acceleration = new float[3];

    private EditText input_ip, input_port;
    Button bt_connect, bt_exit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Screen orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        mPreview = new FeatureStreamingCameraPreview(this, fs, (SurfaceView) findViewById(R.id.surfaceView));

        mPreview.setLayoutParams(new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT));

        ((FrameLayout) findViewById(R.id.layout)).addView(mPreview);
        mPreview.setKeepScreenOn(true);

//        setContentView(mPreview);
        numberOfCameras = Camera.getNumberOfCameras();

        // Find the ID of the default camera
        CameraInfo cameraInfo = new CameraInfo();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                defaultCameraId = i;
            }
        }

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        input_ip = findViewById(R.id.in_addr);
        input_port = findViewById(R.id.in_port);

        bt_connect = findViewById(R.id.bt_connect);
        bt_exit = findViewById(R.id.bt_close);


        //Load last ip address in EditText
        input_ip.setText(loadIPAddressPref(this));

        //Load last port in EditText
        input_port.setText(loadPortAddressPref(this));

        final Context activityContext = (Context) this;

        bt_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String addr = input_ip.getText().toString();
                String portString = input_port.getText().toString();
                int port = Integer.parseInt(portString);

                saveIPAddressPref(activityContext, addr);
                savePortAddressPref(activityContext, portString);

                fs.connect(addr, port);
            }
        });

        bt_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });

    }

    static String loadIPAddressPref(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String prefix = prefs.getString("ipaddr", null);
        if (prefix != null) {
            return prefix;
        } else {
            return "";
        }
    }

    static void saveIPAddressPref(Context context, String text) {
        SharedPreferences.Editor prefs = context
                .getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putString("ipaddr", text);
        prefs.commit();
    }

    static String loadPortAddressPref(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String prefix = prefs.getString("port", null);
        if (prefix != null) {
            return prefix;
        } else {
            return "";
        }
    }

    static void savePortAddressPref(Context context, String text) {
        SharedPreferences.Editor prefs = context
                .getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putString("port", text);
        prefs.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_FASTEST);

        mCamera = Camera.open();
        cameraCurrentlyLocked = defaultCameraId;
        mPreview.setCamera(mCamera);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);

        if (mCamera != null) {
            mPreview.setCamera(null);
            mCamera.release();
            mCamera = null;
        }
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        final float alpha = 0.8f;

        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

        linear_acceleration[0] = event.values[0] - gravity[0];
        linear_acceleration[1] = event.values[1] - gravity[1];
        linear_acceleration[2] = event.values[2] - gravity[2];

        mPreview.updateAccelerometerFeatures(linear_acceleration);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

}
